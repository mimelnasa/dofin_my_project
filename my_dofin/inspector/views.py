import os
import re
import datetime
import subprocess
from django.conf import settings
from django.shortcuts import render, redirect
import uuid # Para identificadores únicos
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from django.core.mail import *
# Importar los modelos
from .models import Petition, Product, Seller, SellerFeedback, CamelProduct
# Importar los formularios
from .forms import PetitionModelForm, ContactForm

# Se definen directorios de trabajo
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SPIDERS_DIR = os.path.join(os.path.dirname(BASE_DIR), "rastreathor", "rastreathor", "spiders")
URL_CAMEL_BASE = 'https://es.camelcamelcamel.com/search?sq='

# MUY IMPORTANTE  ==> Es el identificador de UNA búsqueda concreta, que permitirá relacionar las arañas
UNIQUE_ID = None

# Varios formatos de fechas
DATE_INPUT_FORMATS = (
     '%Y-%m-%d', '%m/%d/%Y', '%m/%d/%y', '%b %d %Y', '%b %d, %Y', '%d %b %Y', 
     '%d %b, %Y', '%B %d %Y', '%B %d, %Y', '%d %B %Y', '%d %B, %Y',              
)

def index(request):
    petition_form = PetitionModelForm(request.POST or None)
    # Conteo del número de visitas en la sesión.    
    num_visits = request.session.get('num_visits', 0)
    request.session['num_visits'] = num_visits + 1


    # SE COMPRUEBA VALIDEZ DEL FORMULARIO
    if petition_form.is_valid():

        # MUY IMPORTANTE  ==> Es el identificador de UNA búsqueda concreta, que permitirá relacionar las arañas
        global UNIQUE_ID
        UNIQUE_ID = uuid.uuid4()

        url_introduced = petition_form.cleaned_data.get("urlpetition")

        #Dividir la URL
        protocol = re.search(r'^https?',url_introduced)[0] if bool(re.search(r'^https?',url_introduced)) else ''
        has_predomain = bool(re.search(r'^(([^\.]*)\.){2,}([^/]*)/',url_introduced))
        predomain_aux = re.search(r'^([^\.]*)\.',url_introduced.replace(protocol+'://',''))[0] if has_predomain else ""
        predomain = predomain_aux[:-1]
        domain_aux = re.search(r'^([^/]*)/?',url_introduced.replace(protocol+'://'+predomain_aux,''))[0]
        domain = domain_aux[:-1] if domain_aux[-1]=='/' else domain_aux

        #Nueva petición con los elementos divididos para el Modelo
        new_petition = Petition.objects.create( id = UNIQUE_ID, 
                                                urlpetition=url_introduced, 
                                                protocol=protocol, 
                                                predomain=predomain, 
                                                domain=domain)
    
        context = {
            'num_visits':num_visits,
            'petition_form':petition_form,
        }

        # Cambio a la vista de carga
        return redirect(loading)

    else:
        context = {
            'num_visits':num_visits,
            'petition_form':petition_form,
        }
        # Pinta el index.html
        return render(request, 'index.html', context)



def loading(request):  
    context = {
        "tab_title": "Cargando...",
    }
    return render(request, 'loading.html', context)


def solution(request):    
    url_introduced = Petition.objects.filter(id = UNIQUE_ID).values_list('urlpetition', flat=True).first()
    domain = Petition.objects.filter(id = UNIQUE_ID).values_list('domain', flat=True).first()

    ###################################################################################################
    ######################################### SCRAPY STUFF ############################################
    # SE EJECUTA LA PRODUCT SPIDER
    p_product = lanzaSpider('crawl_product', UNIQUE_ID, url_introduced, domain)

    # SE ESPERA A QUE SE OBTENGAN LOS DATOS DEL PRODUCTO (SE NECESITA LA URL DEL VENDEDOR)
    p_product.communicate()

    # SE EJECUTA LA CAMEL SPIDER CON LA URL FORMADA DE MARCA + MODELO
    marca = Product.objects.filter(id = UNIQUE_ID).values_list('marca', flat=True).first()
    modelo = Product.objects.filter(id = UNIQUE_ID).values_list('modelo', flat=True).first()
    if marca and modelo:
         # SE EJECUTA LA SELLER SPIDER CON LA URL OBTENIDA
        modelo = modelo.split(';')[0]
        camel_url = (URL_CAMEL_BASE+marca+'+'+modelo).replace(' ','+')
        p_camel = lanzaSpider('crawl_camel', UNIQUE_ID, camel_url, domain)
    # Si NO hay marca Y modelo, no se busca el producto en CamelCamelCamel. 

    seller_url = Product.objects.filter(id = UNIQUE_ID).values_list('seller_url', flat=True).first()
    if seller_url:
        p_seller = lanzaSpider('crawl_seller', UNIQUE_ID, seller_url, domain)
        # Y SE ESPERA A QUE SE OBTENGAN LOS DATOS DEL VENDEDOR (SE NECESITA LA URL DEL FEEDBACK)
        p_seller.communicate()


    # SE EJECUTA LA FEEDBACK SPIDER CON LA URL OBTENIDA        
    feedback_url = Seller.objects.filter(id = UNIQUE_ID).values_list('feedback_url', flat=True).first()
    if feedback_url:
        p_sellerfeedback = lanzaSpider('crawl_sellerfeedback', UNIQUE_ID, feedback_url, domain)


    # SE ESPERA A QUE SE EJECUTEN LAS SPIDERS QUE QUEDEN PARA TERMINAR
    try:
        p_sellerfeedback.communicate()
    except:
        print("====> NO SE INICIÓ ARAÑA SELLERFEEDBACK")
    
    try:
        p_camel.communicate()
    except:
        my_product = Product.objects.get(id=UNIQUE_ID)
        my_product.ind_NO_CAMEL = 1    # se actualiza el campo
        my_product.save()              # y se guarda el cambio
        print("=====> NO SE INICIÓ ARAÑA CAMEL")
        
    ###################################################################################################
    ###################################################################################################

    return render( request, 'solution.html', prepareContext(marca,modelo) )


def prepareContext(marca, modelo):
    title = 'Informe DOFIN'
    tab_title = 'Informe'

    FLAG_NO_CAMEL = Product.objects.filter(id = UNIQUE_ID).values_list('ind_NO_CAMEL', flat=True).first()
    FLAG_NO_PRODUCT = CamelProduct.objects.filter(id = UNIQUE_ID).values_list('ind_NO_PRODUCT', flat=True).first()
    precio = Product.objects.filter(id = UNIQUE_ID).values_list('precio', flat=True).first()
    highest_amazon_price = CamelProduct.objects.filter(id = UNIQUE_ID).values_list('highest_amazon_price', flat=True).first()
    highest_amazon_date = CamelProduct.objects.filter(id = UNIQUE_ID).values_list('highest_amazon_date', flat=True).first()
    lowest_amazon_price = CamelProduct.objects.filter(id = UNIQUE_ID).values_list('lowest_amazon_price', flat=True).first()
    lowest_amazon_date = CamelProduct.objects.filter(id = UNIQUE_ID).values_list('lowest_amazon_date', flat=True).first()
    avg_amazon_price = CamelProduct.objects.filter(id = UNIQUE_ID).values_list('avg_amazon_price', flat=True).first()
    highest_tercero_price = CamelProduct.objects.filter(id = UNIQUE_ID).values_list('highest_tercero_price', flat=True).first()
    highest_tercero_date = CamelProduct.objects.filter(id = UNIQUE_ID).values_list('highest_tercero_date', flat=True).first()
    lowest_tercero_price = CamelProduct.objects.filter(id = UNIQUE_ID).values_list('lowest_tercero_price', flat=True).first()
    lowest_tercero_date = CamelProduct.objects.filter(id = UNIQUE_ID).values_list('lowest_tercero_date', flat=True).first()
    avg_tercero_price = CamelProduct.objects.filter(id = UNIQUE_ID).values_list('avg_tercero_price', flat=True).first()

    
    FLAG_PRICE_ALERT = 0  # Se comprueba si el precio está entre máximo y mínimo
    if not FLAG_NO_CAMEL and not FLAG_NO_PRODUCT:
        if not none_cast_int(lowest_amazon_price)<=precio<=none_cast_int(highest_amazon_price):
            FLAG_PRICE_ALERT = 1
        if not none_cast_int(lowest_tercero_price)<=precio<=none_cast_int(highest_tercero_price):
            FLAG_PRICE_ALERT = 1

    fecha_alta = Seller.objects.filter(id = UNIQUE_ID).values_list('antiguedad', flat=True).first() 
    date_fecha_alta = None
    for date_format in DATE_INPUT_FORMATS :
        try:
            date_fecha_alta = datetime.datetime.strptime(fecha_alta, date_format) 
            break 
        except ValueError:
            pass # Si no es formato correcto, intento seguir buscando
    if date_fecha_alta:
        FLAG_DAYS_ANTIQ = (datetime.datetime.now() - date_fecha_alta).days
    else:
        FLAG_DAYS_ANTIQ = -1

    print("88888888888888888888888888888888888888888888888888")
    print(FLAG_DAYS_ANTIQ)
    print("88888888888888888888888888888888888888888888888888")

    num_positives_1month = SellerFeedback.objects.filter(id = UNIQUE_ID).values_list('num_positives_1month', flat=True).first()
    num_negatives_1month = SellerFeedback.objects.filter(id = UNIQUE_ID).values_list('num_negatives_1month', flat=True).first()
    num_neutrals_1month = SellerFeedback.objects.filter(id = UNIQUE_ID).values_list('num_neutrals_1month', flat=True).first()
    num_positives_6months = SellerFeedback.objects.filter(id = UNIQUE_ID).values_list('num_positives_6months', flat=True).first()
    num_negatives_6months = SellerFeedback.objects.filter(id = UNIQUE_ID).values_list('num_negatives_6months', flat=True).first()
    num_neutrals_6months = SellerFeedback.objects.filter(id = UNIQUE_ID).values_list('num_neutrals_6months', flat=True).first()
    num_positives_12months = SellerFeedback.objects.filter(id = UNIQUE_ID).values_list('num_positives_12months', flat=True).first()
    num_negatives_12months = SellerFeedback.objects.filter(id = UNIQUE_ID).values_list('num_negatives_12months', flat=True).first()
    num_neutrals_12months = SellerFeedback.objects.filter(id = UNIQUE_ID).values_list('num_neutrals_12months', flat=True).first()

    FLAG_WORSE_FEEDBACK = 0  # Se comprueba si el ratio de valoraciones del último mes EMPEORA el semestre y el año
    if num_positives_1month and num_negatives_1month: #Esxistiendo estos, debería existir el resto de valores
        # Se suma 1 para evitar los CEROS
        if (num_positives_1month+1)/(num_negatives_1month+1) < (num_positives_6months+1)/(num_negatives_6months+1) < (num_positives_12months+1)/(num_negatives_12months+1):
            FLAG_WORSE_FEEDBACK = 1


    vendidos = Product.objects.filter(id = UNIQUE_ID).values_list('vendidos', flat=True).first()
    FLAG_SOLDS = 1 if none_cast_int(vendidos)>100 else 0

    followers = Seller.objects.filter(id = UNIQUE_ID).values_list('seguidores', flat=True).first()
    FLAG_FOLLOWERS = 1 if none_cast_int(followers)>200 else 0
    

    context = {
        # --------------------- PRODUCTO ---------------------------------------
        "title": title,
        "tab_title": tab_title,
        "descripcion": Product.objects.filter(id = UNIQUE_ID).values_list('descripcion', flat=True).first(),
        "estado": Product.objects.filter(id = UNIQUE_ID).values_list('estado', flat=True).first(),
        "marca": marca,
        "modelo": modelo,
        "FLAG_NO_CAMEL": FLAG_NO_CAMEL,
        "FLAG_NO_PRODUCT": FLAG_NO_PRODUCT,
        "precio": precio,
        "FLAG_PRICE_ALERT": FLAG_PRICE_ALERT,
        "divisa": Product.objects.filter(id = UNIQUE_ID).values_list('divisa', flat=True).first(),
        "vendidos": vendidos,

        "highest_amazon_price": highest_amazon_price,
        "highest_amazon_date": highest_amazon_date,
        "lowest_amazon_price": lowest_amazon_price,
        "lowest_amazon_date": lowest_amazon_date,
        "avg_amazon_price": avg_amazon_price,
        "highest_tercero_price": highest_tercero_price,
        "highest_tercero_date": highest_tercero_date,
        "lowest_tercero_price": lowest_tercero_price,
        "lowest_tercero_date": lowest_tercero_date,
        "avg_tercero_price": avg_tercero_price,

        "FLAG_PAYPAL": Product.objects.filter(id = UNIQUE_ID).values_list('ind_paypal', flat=True).first(),
        "FLAG_MASTERCARD": Product.objects.filter(id = UNIQUE_ID).values_list('ind_mastercard', flat=True).first(),
        "FLAG_VISA": Product.objects.filter(id = UNIQUE_ID).values_list('ind_visa', flat=True).first(),
        "FLAG_DISCOVER": Product.objects.filter(id = UNIQUE_ID).values_list('ind_discover', flat=True).first(),


        "URL_graph": CamelProduct.objects.filter(id = UNIQUE_ID).values_list('URL_graph', flat=True).first(),


        # --------------------- VENDEDOR ---------------------------------------
        "seller_name": Seller.objects.filter(id = UNIQUE_ID).values_list('nombre', flat=True).first(),
        "seller_direction": Seller.objects.filter(id = UNIQUE_ID).values_list('direccion', flat=True).first(),
        "seller_country": Seller.objects.filter(id = UNIQUE_ID).values_list('pais', flat=True).first(),
        "seller_email": Seller.objects.filter(id = UNIQUE_ID).values_list('email', flat=True).first(),
        "FLAG_NO_PHOTO": Seller.objects.filter(id = UNIQUE_ID).values_list('ind_no_foto', flat=True).first(),
        "seller_antiquity": fecha_alta,
        "FLAG_DAYS_ANTIQ": FLAG_DAYS_ANTIQ,
        "followers": followers,
        "FLAG_FOLLOWERS": FLAG_FOLLOWERS,
        "products_for_sale": Seller.objects.filter(id = UNIQUE_ID).values_list('articulos_en_venta', flat=True).first(),

        "user_name": SellerFeedback.objects.filter(id = UNIQUE_ID).values_list('user_name', flat=True).first(),
        "PS_FLAG": SellerFeedback.objects.filter(id = UNIQUE_ID).values_list('indicador_PS', flat=True).first(),
        "feedback_num_total": SellerFeedback.objects.filter(id = UNIQUE_ID).values_list('feedback_num_total', flat=True).first(),

        "num_positives_1month": num_positives_1month,
        "num_negatives_1month": num_negatives_1month,
        "num_neutrals_1month": num_neutrals_1month,
        "num_positives_6months": num_positives_6months,
        "num_negatives_6months": num_negatives_6months,
        "num_neutrals_6months": num_neutrals_6months,
        "num_positives_12months": num_positives_12months,
        "num_negatives_12months": num_negatives_12months,
        "num_neutrals_12months": num_neutrals_12months,

        "FLAG_WORSE_FEEDBACK" :FLAG_WORSE_FEEDBACK,

        # -------------------- Compra-Venta -----------------------------------
        "term_return": Product.objects.filter(id = UNIQUE_ID).values_list('term_return', flat=True).first(),
        "shipping_costs": Product.objects.filter(id = UNIQUE_ID).values_list('shipping_costs', flat=True).first(),


    }
    return context

def none_cast_int(result):
    if result is None:
        result = -1
    return result

 
def contact(request):
    title = 'Contacto'
    tab_title = 'CONTÁCTANOS'
    contact_form = ContactForm(request.POST or None)

    if contact_form.is_valid():
        name = contact_form.cleaned_data.get("name")
        email = contact_form.cleaned_data.get("email")
        subject = 'BUZÓN|' + contact_form.cleaned_data.get("subject")
        message = contact_form.cleaned_data.get("message")

        if not subject:
            subject = 'BUZÓN|Sin asunto'
            # ORDEN EMAIL: asunto, mensaje, remitente, destinatario
            send_mail(subject, message, settings.EMAIL_HOST_USER, [settings.EMAIL_HOST_USER], fail_silently=False)
            #mail = EmailMessage(subject, message, to=[settings.EMAIL_HOST_USER])
            #, fail_silently=False
            #mail.send()
            #return HttpResponseRedirect('/')
    context = {
        "title": title,
        "tab_title": tab_title,
        "contact_form": contact_form,
    }
    return render(request, 'contact.html',context)


def faq(request):    
    title = 'Preguntas Frecuentes'
    tab_title = 'FAQ'
    context = {
        "title": title,
        "tab_title": tab_title,
    }
    return render(request, 'FAQ.html', context)


def about(request):    
    title = 'Acerca de DOFIN'
    tab_title = 'ACERCA'
    context = {
        "title": title,
        "tab_title": tab_title,
    }
    return render(request, 'about.html', context)

# def login(request):
#     title = 'SIGN IN'
#     subtitle = 'to continue'
#     tab_title = 'Login'
#     context = {
#         "title": title,
#         "subtitle": subtitle,
#         "tab_title": tab_title,
#     }
#     return render(request, 'login.html', context)


############################################################################
                        # LANZAMIENTO SPIDERS
############################################################################
def lanzaSpider(spider_name, unique_id, url_introduced, domain):
    args = '-a url={url} -a domain={domain} -a id={id}'.format(
            url = url_introduced
            ,domain = domain
            ,id = unique_id
        )
    
    execution = 'scrapy crawl {spider} {args}'.format(
            spider = spider_name
            ,args = args
        )

    p = subprocess.Popen(execution,
                        cwd = SPIDERS_DIR )
    return p