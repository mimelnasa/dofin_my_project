from django.conf.urls import url

from . import views
  

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^about$', views.about,  name='about'),    
    url(r'^contact$', views.contact, name='contact'),
    url(r'^FAQ$', views.faq, name='faq'),
    url(r'^loading$', views.loading, name='loading'),
    url(r'^solution$', views.solution, name='solution'),

    # url(r'^api/crawl$', views.crawl, name='crawl'),
]