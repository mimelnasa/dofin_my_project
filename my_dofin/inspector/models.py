import uuid # Para identificadores únicos
from django.utils import timezone
from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.

class Petition(models.Model):
	id = models.UUIDField(primary_key=True, default=uuid.uuid4, help_text="ID único para esta petición concreta.")
	urlpetition = models.URLField()	
	protocol = models.CharField(default=urlpetition,max_length=10, help_text=_("Concepto de la URL objeto de estudio."))
	predomain = models.CharField(default=urlpetition,max_length=100, help_text="Concepto de la URL objeto de estudio.")
	domain = models.CharField(default=urlpetition,max_length=1000, help_text="Concepto de la URL objeto de estudio.")
	# email = models.EmailField(blank=True, null=True)
	timestamp = models.DateTimeField(auto_now_add=True)
	# ind_instant_message = models.BooleanField(default=False)
	# ind_post = models.BooleanField(default=False)
	# ind_advertisement = models.BooleanField(default=False)
	# ind_googling = models.BooleanField(default=False)

	def __str__(self):
		return self.domain


class Product(models.Model):
	timestamp = models.DateTimeField(auto_now_add=True)
	id = models.UUIDField(primary_key=True, default=uuid.uuid4, help_text=_("ID único para esta petición concreta."))
	descripcion = models.CharField(max_length=1000, help_text=_("Descripción principal de producto."))	
	estado  = models.CharField(null=True, max_length=100, help_text=_("Modelo del producto."))
	marca = models.CharField(null=True, max_length=100, help_text=_("Marca del producto."))
	modelo  = models.CharField(null=True, max_length=100, help_text=_("Modelo del producto."))
	ind_NO_CAMEL = models.IntegerField(null=True, default=0, help_text=_("Indicador de resolución en CamelCamelCamel."))
	precio = models.FloatField(null=True, help_text=_("Precio del producto."))
	divisa = models.CharField(null=True, max_length=100, help_text=_("Divisa del precio del producto."))
	vendidos = models.IntegerField(null=True, help_text=_("Número de artículos vendidos por el vendedor del mismo producto."))
	media_valoraciones = models.FloatField(null=True, help_text=_("Media de las valoraciones por los usuarios."))
	# Info del vendedor
	seller_name = models.CharField(null=True, max_length=100, help_text=_("Nombre de usuario del vendedor del producto."))
	seller_url = models.URLField(null=True, help_text=_("Perfil del vendedor del producto."))
	# Info de la compraventa
	term_return = models.IntegerField(null=True, help_text=_("Plazo de devolución en días."))
	shipping_costs = models.FloatField(null=True, help_text=_("Coste de envío."))
	ind_paypal = models.IntegerField(null=True, help_text=_("Indicador permite pago con PayPal."))
	ind_mastercard = models.IntegerField(null=True, help_text=_("Indicador permite pago con Master Card."))
	ind_visa = models.IntegerField(null=True, help_text=_("Indicador permite pago con Visa."))
	ind_discover = models.IntegerField(null=True, help_text=_("Indicador permite pago con Discover."))

	def __str__(self):
		return self.descripcion
	

class Seller(models.Model):
	timestamp = models.DateTimeField(auto_now_add=True)
	id = models.UUIDField(primary_key=True, default=uuid.uuid4, help_text=_("ID único para esta petición concreta."))
	nombre = models.CharField(null=True, max_length=100, help_text=_("Nombre completo del vendedor del producto."))
	direccion = models.CharField(null=True, max_length=500, help_text=_("Dirección del vendedor del producto."))
	pais = models.CharField(null=True, max_length=100, help_text=_("País del vendedor del producto."))
	antiguedad = models.CharField(null=True, max_length=100, help_text=_("Fecha de alta del vendedor del producto."))
	email = models.EmailField(blank=True, null=True,  help_text=_("Email del vendedor del producto."))
	seguidores = models.IntegerField(null=True, help_text=_("Número de seguidores del vendedor."))
	articulos_en_venta = models.IntegerField(null=True, help_text=_("Número de artículos en venta del vendedor."))
	feedback_url = models.URLField(null=True, help_text=_("Perfil de votos del vendedor del producto."))
	ind_no_foto = models.IntegerField(null=True, help_text=_("Indicador no foto de perfil."))

	def __str__(self):
		return self.nombre


class SellerFeedback(models.Model):
	timestamp = models.DateTimeField(auto_now_add=True)
	id = models.UUIDField(primary_key=True, default=uuid.uuid4, help_text=_("ID único para esta petición concreta."))
	user_name = models.CharField(null=True, max_length=100, help_text=_("Nombre de usuario del vendedor del producto."))
	indicador_PS = models.IntegerField(null=True, help_text=_("Indicador de vendedor con categoría de POWER SELLER."))
	feedback_num_total = models.IntegerField(null=True, help_text=_("Número de valoraciones totales por los usuarios."))
	# Valoraciones
	num_positives_1month= models.IntegerField(null=True, help_text=_("Número de valoraciones positivas por los usuarios en el último mes."))
	num_negatives_1month = models.IntegerField(null=True, help_text=_("Número de valoraciones neutrales por los usuarios en el último mes."))
	num_neutrals_1month = models.IntegerField(null=True, help_text=_("Número de valoraciones negativas por los usuarios en el último mes."))
	num_positives_6months = models.IntegerField(null=True, help_text=_("Número de valoraciones positivas por los usuarios en los últimos 6 meses."))
	num_negatives_6months = models.IntegerField(null=True, help_text=_("Número de valoraciones neutrales por los usuarios en los últimos 6 meses."))
	num_neutrals_6months = models.IntegerField(null=True, help_text=_("Número de valoraciones negativas por los usuarios en los últimos 6 meses."))
	num_positives_12months = models.IntegerField(null=True, help_text=_("Número de valoraciones positivas por los usuarios en los últimos 12 meses."))
	num_negatives_12months = models.IntegerField(null=True, help_text=_("Número de valoraciones neutrales por los usuarios en los últimos 12 meses."))
	num_neutrals_12months = models.IntegerField(null=True, help_text=_("Número de valoraciones negativas por los usuarios en los últimos 12 meses."))

	def __str__(self):
		return self.user_name


class CamelProduct(models.Model):
	timestamp = models.DateTimeField(auto_now_add=True)
	id = models.UUIDField(primary_key=True, default=uuid.uuid4, help_text=_("ID único para esta petición concreta."))
	descripcion = models.CharField(null=True, max_length=1000, help_text=_("Descripción principal de producto."))
	ind_NO_PRODUCT = models.IntegerField(null=True, default=0, help_text=_("Indicador de existencia del producto."))
	# Precios Amazon
	highest_amazon_price = models.FloatField(null=True, help_text=_("Precio máximo del producto de Amazon en Amazon."))
	highest_amazon_date = models.CharField(null=True, max_length=100, help_text=_("Fecha del precio máximo del producto de Amazon en Amazon."))
	lowest_amazon_price = models.FloatField(null=True, help_text=_("Precio mínimo del producto de Amazon en Amazon."))
	lowest_amazon_date = models.CharField(null=True, max_length=100, help_text=_("Fecha del precio mínimo del producto de Amazon en Amazon."))
	avg_amazon_price = models.FloatField(null=True, help_text=_("Precio medio del producto de Amazon en Amazon."))
	# Precios terceros en Amazon
	highest_tercero_price = models.FloatField(null=True, help_text=_("Precio máximo del producto de terceros en Amazon."))
	highest_tercero_date = models.CharField(null=True, max_length=100, help_text=_("Fecha del precio mínimo del producto de terceros en Amazon."))
	lowest_tercero_price = models.FloatField(null=True, help_text=_("Precio mínimo del producto de terceros en Amazon."))
	lowest_tercero_date = models.CharField(null=True, max_length=100, help_text=_("Fecha del precio mínimo del producto de terceros en Amazon."))
	avg_tercero_price = models.FloatField(null=True, help_text=_("Precio medio del producto de terceros en Amazon."))
	# Gráfica del variabilidad del precio del producto en Amazon
	URL_graph = models.URLField(null=True, help_text=_("Imagen del gráfico de CamelCamelCamel."))	
	SKU_graph = models.CharField(null=True, max_length=1000, help_text=_("Clave de producto para gráfica."))

	def __str__(self):
		return self.descripcion