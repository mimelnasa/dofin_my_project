
from scrapy.utils.project import get_project_settings
from scrapy.spiders import Spider, Rule
from scrapy.linkextractors import LinkExtractor
from rastreathor.items import ProductItem

class EbayProductSpider(Spider):
	name = 'crawl_product'
	
	def __init__(self, *args, **kwargs):
		self.url = kwargs.get('url')
		self.domain = kwargs.get('domain')		
		self.id = kwargs.get('id')       
		self.start_urls = [self.url]
		self.allowed_domains = [self.domain]

	def parse(self, response):
		new_product_item = ProductItem()
		#INFO DEL PRODUCTO
		new_product_item['id'] = self.id
		new_product_item['descripcion'] = response.xpath('normalize-space(//*[@id="itemTitle"]/text())').get()
		new_product_item['estado'] = response.xpath('//*[@id="vi-itm-cond"]/text()').get()
		new_product_item['marca'] = response.xpath('normalize-space(//td[contains(translate(text(),"ABCDEFGHIJKLMNOPURSTUWXYZ","abcdefghijklmnopurstuwxyz") ,"marca") \
																		or contains(translate(text(),"ABCDEFGHIJKLMNOPURSTUWXYZ","abcdefghijklmnopurstuwxyz") ,"brand") \
																		or contains(translate(text(),"ABCDEFGHIJKLMNOPURSTUWXYZ","abcdefghijklmnopurstuwxyz") ,"marque") \
																		or contains(translate(text(),"ABCDEFGHIJKLMNOPURSTUWXYZ","abcdefghijklmnopurstuwxyz") ,"marke")]/following-sibling::*[1])').get()
		
		model_aux_list = response.xpath('normalize-space(//td[contains(translate(text(),"ABCDEFGHIJKLMNOPURSTUWXYZ","abcdefghijklmnopurstuwxyz") ,"mpn") or contains(translate(text(),"ABCDEFGHIJKLMNOPURSTUWXYZè","abcdefghijklmnopurstuwxyze") ,"model")]/following-sibling::*[1])').getall()
		# Se recorren todos los posibles modelos obtenidos
		for model in model_aux_list:
			if model.lower().strip()=='does not apply' or model.lower().strip()=='no aplicable' or model.lower().strip()=='sin mpn':
				model_aux_list.remove(model)
		model_aux_list = [model.strip() for model in model_aux_list]
		new_product_item['modelo'] = ';'.join(model_aux_list)
		
		new_product_item['precio'] = response.xpath('normalize-space(//*[@id="prcIsum"]/text())').get() or -1.0
		new_product_item['divisa'] = new_product_item['precio']
		new_product_item['vendidos'] = response.xpath('normalize-space(//a[@class="vi-txt-underline"])').get() 
		new_product_item['media_valoraciones'] = response.xpath('normalize-space(//*[@class="ebay-review-start-rating"]/text())').get()
		
		# INFO DEL VENDEDOR
		new_product_item['seller_name'] = response.xpath('normalize-space(//*[@class="mbg-nw"]/text())').get() 
		new_product_item['seller_url'] = response.xpath('normalize-space(//div[contains(@class,"mbg")]/a/@href)').get() 

		# INFO DE LA TRANSACCION		
		new_product_item['term_return'] = response.xpath('normalize-space(//span[@id="vi-ret-accrd-txt"])').get() 
		# new_product_item['shipping_costs'] = response.xpath('normalize-space(//span[@id="fshippingCost"])').get() or -1.0
		new_product_item['ind_paypal'] = response.xpath('count(//div[@id="payDet1"]//img[@title="PayPal"])').get() 
		new_product_item['ind_mastercard'] = response.xpath('count(//div[@id="payDet1"]//img[translate(@title," ","")="MasterCard"])').get() 
		new_product_item['ind_visa'] = response.xpath('count(//div[@id="payDet1"]//img[@title="Visa"])').get() 
		new_product_item['ind_discover'] = response.xpath('count(//div[@id="payDet1"]//img[@title="Discover"])').get() 


		return new_product_item
