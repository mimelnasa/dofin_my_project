
from scrapy.utils.project import get_project_settings
from scrapy.spiders import Spider, Rule
from scrapy.linkextractors import LinkExtractor
from rastreathor.items import SellerFeedbackItem

class EbaySellerFeedbackSpider(Spider):
	name = 'crawl_sellerfeedback'

	def __init__(self, *args, **kwargs):
		self.url = kwargs.get('url')
		self.domain = kwargs.get('domain')	
		self.id = kwargs.get('id')       
		self.start_urls = [self.url]
		self.allowed_domains = [self.domain]

 #        IcrawlerSpider.rules = [
 #           Rule(LinkExtractor(unique=True), callback='parse_item'),
 #        ]
 #        super(IcrawlerSpider, self).__init__(*args, **kwargs)

	# rules = {
	# # 	# Para cada item
	#  	Rule(LinkExtractor(unique=True,
	# 						callback = 'parse_items', follow = True))
	# }

	def parse(self, response):
		new_sellerfeedback_item = SellerFeedbackItem()
		#info eBay
		print("ESTOY PARSEANDO SELLERFEEDBACK")
		print("============ID================%s" %self.id)
		new_sellerfeedback_item['id'] = self.id
		new_sellerfeedback_item['user_name'] = response.xpath('normalize-space(//*[contains(@class,"mbg")]/text())').get()

		new_sellerfeedback_item['num_positives_12months'] = response.xpath('normalize-space(//tr[@class="fbsSmallYukon"]/td[contains(translate(text(),"ABCDEFGHIJKLMNOPURSTUWXYZ","abcdefghijklmnopurstuwxyz"),"positiv")]/following-sibling::*[3])').get() or \
															response.xpath('normalize-space((//section[@class="overall-rating-summary"]//button[@*[contains(translate(.,"ABCDEFGHIJKLMNOPURSTUWXYZ","abcdefghijklmnopurstuwxyz"),"positiv")]])[3])')
		new_sellerfeedback_item['num_negatives_12months'] = response.xpath('normalize-space(//tr[@class="fbsSmallYukon"]/td[contains(translate(text(),"ABCDEFGHIJKLMNOPURSTUWXYZ","abcdefghijklmnopurstuwxyz"),"neg")]/following-sibling::*[3])').get() or \
															response.xpath('normalize-space((//section[@class="overall-rating-summary"]//button[@*[contains(translate(.,"ABCDEFGHIJKLMNOPURSTUWXYZ","abcdefghijklmnopurstuwxyz"),"neg")]])[3])')
		new_sellerfeedback_item['num_neutrals_12months'] = response.xpath('normalize-space(//tr[@class="fbsSmallYukon"]/td[contains(translate(text(),"ABCDEFGHIJKLMNOPURSTUWXYZ","abcdefghijklmnopurstuwxyz"),"neutr")]/following-sibling::*[3])').get() or \
															response.xpath('normalize-space((//section[@class="overall-rating-summary"]//button[@*[contains(translate(.,"ABCDEFGHIJKLMNOPURSTUWXYZ","abcdefghijklmnopurstuwxyz"),"neutr")]])[3])')
		new_sellerfeedback_item['num_positives_6months'] = response.xpath('normalize-space(//tr[@class="fbsSmallYukon"]/td[contains(translate(text(),"ABCDEFGHIJKLMNOPURSTUWXYZ","abcdefghijklmnopurstuwxyz"),"positiv")]/following-sibling::*[2])').get() or \
															response.xpath('normalize-space((//section[@class="overall-rating-summary"]//button[@*[contains(translate(.,"ABCDEFGHIJKLMNOPURSTUWXYZ","abcdefghijklmnopurstuwxyz"),"positiv")]])[2])')
		new_sellerfeedback_item['num_negatives_6months'] = response.xpath('normalize-space(//tr[@class="fbsSmallYukon"]/td[contains(translate(text(),"ABCDEFGHIJKLMNOPURSTUWXYZ","abcdefghijklmnopurstuwxyz"),"neg")]/following-sibling::*[2])').get() or \
															response.xpath('normalize-space((//section[@class="overall-rating-summary"]//button[@*[contains(translate(.,"ABCDEFGHIJKLMNOPURSTUWXYZ","abcdefghijklmnopurstuwxyz"),"neg")]])[2])')
		new_sellerfeedback_item['num_neutrals_6months'] = response.xpath('normalize-space(//tr[@class="fbsSmallYukon"]/td[contains(translate(text(),"ABCDEFGHIJKLMNOPURSTUWXYZ","abcdefghijklmnopurstuwxyz"),"neutr")]/following-sibling::*[2])').get() or \
															response.xpath('normalize-space((//section[@class="overall-rating-summary"]//button[@*[contains(translate(.,"ABCDEFGHIJKLMNOPURSTUWXYZ","abcdefghijklmnopurstuwxyz"),"neutr")]])[2])')
		new_sellerfeedback_item['num_positives_1month'] = response.xpath('normalize-space(//tr[@class="fbsSmallYukon"]/td[contains(translate(text(),"ABCDEFGHIJKLMNOPURSTUWXYZ","abcdefghijklmnopurstuwxyz"),"positiv")]/following-sibling::*[1])').get() or \
															response.xpath('normalize-space((//section[@class="overall-rating-summary"]//button[@*[contains(translate(.,"ABCDEFGHIJKLMNOPURSTUWXYZ","abcdefghijklmnopurstuwxyz"),"positiv")]])[1])')
		new_sellerfeedback_item['num_negatives_1month'] = response.xpath('normalize-space(//tr[@class="fbsSmallYukon"]/td[contains(translate(text(),"ABCDEFGHIJKLMNOPURSTUWXYZ","abcdefghijklmnopurstuwxyz"),"neg")]/following-sibling::*[1])').get() or \
															response.xpath('normalize-space((//section[@class="overall-rating-summary"]//button[@*[contains(translate(.,"ABCDEFGHIJKLMNOPURSTUWXYZ","abcdefghijklmnopurstuwxyz"),"neg")]])[1])')
		new_sellerfeedback_item['num_neutrals_1month'] = response.xpath('normalize-space(//tr[@class="fbsSmallYukon"]/td[contains(translate(text(),"ABCDEFGHIJKLMNOPURSTUWXYZ","abcdefghijklmnopurstuwxyz"),"neutr")]/following-sibling::*[1])').get() or \
															response.xpath('normalize-space((//section[@class="overall-rating-summary"]//button[@*[contains(translate(.,"ABCDEFGHIJKLMNOPURSTUWXYZ","abcdefghijklmnopurstuwxyz"),"neutr")]])[1])')
		new_sellerfeedback_item['indicador_PS'] = response.xpath('count(//*[contains(translate(@title,"ABCDEFGHIJKLMNOPURSTUWXYZ","abcdefghijklmnopurstuwxyz"),"powerseller")])').get()

		return new_sellerfeedback_item
